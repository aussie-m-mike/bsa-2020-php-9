<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetChartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|lt:end_date',
            'end_date' => 'required|gt:start_date',
        ];
    }
}
