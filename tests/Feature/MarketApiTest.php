<?php

namespace Tests\Feature;

use App\Entities\User;
use App\Entities\Stock;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MarketApiTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testAddStockValid()
    {
        $user = factory(User::class)->create();
        $start_date = Carbon::now()->addHour(1)->format('Y-m-d H:i:s');
        $price = 9.99;
        
        $stock = new Stock([
            'user_id' => $user->id,
            'price' => $price,
            'start_date' => $start_date
         ]);

        $response = $this->actingAs($user)->json("POST", "/api/stocks", [
            "price" => $stock->price,
            "start_date" => $stock->start_date
        ]);

        $response->assertStatus(201)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJsonFragment(
                [
                    "price" => $price,
                    "start_date" => $start_date
                ]
            );

        $this->assertDatabaseCount('stocks',  1);

        $this->assertDatabaseHas('stocks', array_merge($stock->toArray(), [
            'id' => 1,
            'price' => $price,
            'start_date' => $stock->start_date
        ]));
    }

    public function testAddStockUnauthenticated()
    {
        $user = factory(User::class)->create();
        $start_date = Carbon::now()->addHour(1)->format('Y-m-d H:i:s');
        $price = 9.99;

        $stock = new Stock([
            'user_id' => $user->id,
            'price' => $price,
            'start_date' => $start_date
        ]);

        $response = $this->json("POST", "/api/stocks", [
            "price" => $stock->price,
            "start_date" => $stock->start_date
        ]);

        $response->assertStatus(401)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson(
                [
                    "message" => 'Unauthenticated.',
                ]
            );

        $this->assertDatabaseCount('stocks',  0);
        $this->assertDatabaseMissing('stocks', array_merge($stock->toArray(), [
            'id' => 1,
            'price' => $price,
            'start_date' => $stock->start_date
        ]));
    }

    public function testAddStockInvalidPrice()
    {
        $user = factory(User::class)->create();
        $start_date = Carbon::now()->addHour(1)->format('Y-m-d H:i:s');
        $price = [];

        $stock = new Stock([
            'user_id' => $user->id,
            'price' => $price,
            'start_date' => $start_date
        ]);

        $response = $this->actingAs($user)->json("POST", "/api/stocks", [
            "price" => $stock->price,
            "start_date" => $stock->start_date
        ]);

        $response->assertStatus(422)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson(
                [
                    "message" => 'The given data was invalid.',
                ]
            );

        $this->assertDatabaseCount('stocks',  0);
        $this->assertDatabaseMissing('stocks', array_merge($stock->toArray(), [
            'id' => 1,
            'price' => $price,
            'start_date' => $stock->start_date
        ]));
    }

    public function testAddStockNegativePrice()
    {
        $user = factory(User::class)->create();
        $start_date = Carbon::now()->addHour(1)->format('Y-m-d H:i:s');
        $price = -2;

        $stock = new Stock([
            'user_id' => $user->id,
            'price' => $price,
            'start_date' => $start_date
        ]);

        $response = $this->actingAs($user)->json("POST", "/api/stocks", [
            "price" => $stock->price,
            "start_date" => $stock->start_date
        ]);

        $response->assertStatus(422)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson(
                [
                    "message" => 'The given data was invalid.',
                ]
            );

        $this->assertDatabaseCount('stocks',  0);
        $this->assertDatabaseMissing('stocks', array_merge($stock->toArray(), [
            'id' => 1,
            'price' => $price,
            'start_date' => $stock->start_date
        ]));
    }

    public function testAddStockWithPastDate()
    {
        $user = factory(User::class)->create();
        $start_date = Carbon::now()->subDay(1)->format('Y-m-d H:i:s');
        $price = 9.99;

        $stock = new Stock([
            'user_id' => $user->id,
            'price' => $price,
            'start_date' => $start_date
        ]);

        $response = $this->actingAs($user)->json("POST", "/api/stocks", [
            "price" => $stock->price,
            "start_date" => $stock->start_date
        ]);

        $response->assertStatus(422)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson(
                [
                    "message" => 'The given data was invalid.',
                ]
            );

        $this->assertDatabaseCount('stocks',  0);

        $this->assertDatabaseMissing('stocks', array_merge($stock->toArray(), [
            'id' => 1,
            'price' => $price,
            'start_date' => $stock->start_date
        ]));
    }


    public function testDeleteStockValid()
    {
        $user = factory(User::class)->create();
        $stock = factory(Stock::class)->create(['user_id' => $user->id]);

        $this->assertDatabaseCount('stocks',  1);

        $response = $this->actingAs($user)->json("DELETE", "/api/stocks/{$stock->id}");

        $response->assertStatus(204);

        $this->assertDatabaseCount('stocks',  0);
    }

    public function testDeleteStockUnauthenticated()
    {
        $user = factory(User::class)->create();
        $stock = factory(Stock::class)->create(['user_id' => $user->id]);

        $this->assertDatabaseCount('stocks',  1);

        $response = $this->json("DELETE", "/api/stocks/{$stock->id}");

        $response->assertStatus(401)
            ->assertJson(
            [
                "message" => 'Unauthenticated.',
            ]);

        $this->assertDatabaseCount('stocks',  1);
    }

    public function testDeleteNotMyStock()
    {
        $user = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        $stock = factory(Stock::class)->create(['user_id' => $user->id]);

        $this->assertDatabaseCount('stocks',  1);

        $response = $this->actingAs($secondUser)->json("DELETE", "/api/stocks/{$stock->id}");

        $response->assertStatus(404)->assertJson(
            [
                "message" => 'Stock not found',
            ]
        );

        $this->assertDatabaseCount('stocks',  1);
    }



    public function testGetChartData()
    {
        $start_date = Carbon::now()->subYear(20);
        $end_date = Carbon::now()->addDay(1);
        $frequency = 10;
        $user = factory(User::class)->create();
        factory(Stock::class, 60)->create(['user_id' => $user->id]);

        $this->assertDatabaseCount('stocks',  60);

        $response = $this->json("GET", "/api/chart-data", [
            'start_date' => $start_date->timestamp,
            'end_date' => $end_date->timestamp,
            'frequency' => $frequency
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                '*' => [
                    'date',
                    'price'
                ]
            ]
        ]);
    }

    public function testGetChartDataInvalidRequestData()
    {
        $user = factory(User::class)->create();
        factory(Stock::class)->create(['user_id' => $user->id]);

        $this->assertDatabaseCount('stocks',  1);

        $response = $this->json("GET", "/api/chart-data");

        $response->assertStatus(422);
    }

    public function testGetChartDataStartEndDate()
    {
        $start_date = Carbon::now()->addDay(1);
        $end_date = Carbon::now()->subDay(20);
        $frequency = 1;

        $response = $this->json("GET", "/api/chart-data", [
            'start_date' => $start_date->timestamp,
            'end_date' => $end_date->timestamp,
            'frequency' => $frequency
        ]);

        $response->assertStatus(422)->assertJson(
            [
                "message" => 'The given data was invalid.',
            ]
        );

    }

    public function testFrequency()
    {
        $start_date = Carbon::now()->subDay(20);
        $end_date = Carbon::now()->addDay(20);
        $frequency = -2;

        $response = $this->json("GET", "/api/chart-data", [
            'start_date' => $start_date->timestamp,
            'end_date' => $end_date->timestamp,
            'frequency' => $frequency
        ]);

        $response->assertStatus(400)->assertJson(
            [
                "message" => 'Frequency must be greater than 0',
            ]
        );
    }
}
