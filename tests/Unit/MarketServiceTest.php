<?php

namespace Tests\Unit;

use App\DTO\Collections\ChartDataCollection;
use App\Entities\Stock;
use App\Services\Exceptions\InvalidFrequencyException;
use Carbon\Carbon;
use App\Repositories\StockRepository;
use Tests\TestCase;
use App\Services\MarketDataService;

class MarketServiceTest extends TestCase
{
    public function test_getChartData_returns_data_correctly_by_hour()
    {
        $stockRepository = $this->createMock(StockRepository::class);
        $stockRepository->method('findByCriteria')->willReturn($this->stockList());

        $marketDataService = new MarketDataService($stockRepository);

        $startDate = Carbon::now()->subYear(10);
        $endDate = Carbon::now();
        $frequency = 60*60;

        $result = $marketDataService->getChartData($startDate, $endDate, $frequency);

        $this->assertCount(5, $result);
        $this->assertEquals($result[0]->getPrice(), 7.5);
        $this->assertEquals($result[1]->getPrice(), 7);
        $this->assertEquals($result[2]->getPrice(), 20);
        $this->assertEquals($result[3]->getPrice(), 30);
        $this->assertEquals($result[4]->getPrice(), 50);
    }

    public function test_getChartData_returns_data_correctly_by_day()
    {
        $stockRepository = $this->createMock(StockRepository::class);
        $stockRepository->method('findByCriteria')->willReturn($this->stockList());

        $marketDataService = new MarketDataService($stockRepository);

        $startDate = Carbon::now()->subYear(10);
        $endDate = Carbon::now();
        $frequency = 60*60*24;

        $result = $marketDataService->getChartData($startDate, $endDate, $frequency);

        $this->assertCount(2, $result);
        $this->assertEquals($result[0]->getPrice(), 14.4);
        $this->assertEquals($result[1]->getPrice(), 50);
    }

    public function test_getChartData_returns_data_correctly_by_week()
    {

        $stockRepository = $this->createMock(StockRepository::class);
        $stockRepository->method('findByCriteria')->willReturn($this->stockList());

        $marketDataService = new MarketDataService($stockRepository);

        $startDate = Carbon::now()->subYear(10);
        $endDate = Carbon::now();
        $frequency = 60*60*24*7;

        $result = $marketDataService->getChartData($startDate, $endDate, $frequency);

        $this->assertCount(1, $result);
        $this->assertEquals($result[0]->getPrice(), 20.333333333333);
    }

    public function test_getChartData_returns_data_correctly_if_empty()
    {
        $stockRepository = $this->createMock(StockRepository::class);
        $stockRepository->method('findByCriteria')->willReturn(collect([]));

        $marketDataService = new MarketDataService($stockRepository);

        $startDate = Carbon::now()->subYear(2);
        $endDate = Carbon::now()->subYear(1);
        $frequency = 1;

        $result = $marketDataService->getChartData($startDate, $endDate, $frequency);

        $this->assertInstanceOf(ChartDataCollection::class, $result);
    }

    public function test_getChartData_negative_frequency()
    {
        $stockRepository = $this->createMock(StockRepository::class);
        $marketDataService = new MarketDataService($stockRepository);

        $startDate = Carbon::now()->subHour(1);
        $endDate = Carbon::now();
        $frequency = 0;

        $this->expectException(InvalidFrequencyException::class);
        $this->expectExceptionMessage('Frequency must be greater than 0');

        $marketDataService->getChartData($startDate, $endDate, $frequency);
    }

    private function stockList()
    {
        return collect([
            //2020-07-21 14.4
            factory(Stock::class)->make(['start_date' => Carbon::parse('2020-07-21 10:00:00'), 'price' => 10]),
            factory(Stock::class)->make(['start_date' => Carbon::parse('2020-07-21 10:00:00'), 'price' => 5]),
            factory(Stock::class)->make(['start_date' => Carbon::parse('2020-07-21 12:00:00'), 'price' => 7]),
            factory(Stock::class)->make(['start_date' => Carbon::parse('2020-07-21 13:00:00'), 'price' => 20]),
            factory(Stock::class)->make(['start_date' => Carbon::parse('2020-07-21 14:00:00'), 'price' => 30]),
            factory(Stock::class)->make(['start_date' => Carbon::parse('2020-07-22 10:00:00'), 'price' => 50]),
        ]);
    }
}
